/******************************************************************************** 
 *	Copyright 2014 Monad Solutions Ltd.
 *
 *  	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
********************************************************************************/
package com.monadsolutions.opa.plugin.utils;

import java.util.ArrayList;
import java.util.List;

import com.oracle.determinations.engine.Attribute;
import com.oracle.determinations.engine.Entity;
import com.oracle.determinations.engine.Relationship;
import com.oracle.determinations.server.assess.data.AttributeOutcome;
import com.oracle.determinations.server.assess.data.RelationshipOutcome;
import com.oracle.determinations.server.assess.extensions.AssessPlugin;
import com.oracle.determinations.server.assess.extensions.AssessPluginRegisterArgs;
import com.oracle.determinations.server.assess.extensions.events.OnMapDataEvent;
import com.oracle.determinations.server.assess.extensions.events.OnMapDataEventHandler;

/***
 * Determinations Server plugin to return the session state in addition to
 * the requested outcomes.
 * 
 * @author Alastair Calderwood
 *
 */
public class ODSResponseHandler implements OnMapDataEventHandler {
	
	public AssessPlugin getInstance(AssessPluginRegisterArgs arg0) {
		return new ODSResponseHandler();
	}

	@SuppressWarnings("unchecked")
	public void handleEvent(Object assessEngineSender, OnMapDataEvent event) {

		List<Entity> entities = event.getRulebase().getRulebase().getEntities();

		for (Entity entity : entities) {
			@SuppressWarnings("rawtypes")
			ArrayList outcomeList = new ArrayList();
			List<Attribute> attrs = entity.getAttributes();
			for (Attribute attr : attrs) {
				AttributeOutcome outcome = new AttributeOutcome(attr.getName(), (byte) 0, (byte) 0);
				outcomeList.add(outcome);
			}
			List<Relationship> rels = entity.getRelationships();
			for (Relationship rel : rels) {
				RelationshipOutcome outcome = new RelationshipOutcome(rel.getName(), (byte) 0);
				outcomeList.add(outcome);
			}			
			event.getConfig().addAssessOutcomesForEntity(entity.getName(), outcomeList);
		}
	}
}
